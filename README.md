# Babylonjs Primeros Pasos

## Qué es babylonjs
Babylon.js es un motor 3D en tiempo real que utiliza una biblioteca de JavaScript para mostrar gráficos 3D en un navegador web a través de HTML5. El código fuente está disponible en github y se distribuye bajo la Licencia Apache 2.0.

## Utilizar Babylonjs
Para utilizar esta librería primero se debe asegurar que el navegador objetivo es compatible con WebGl en el siguiente [enlace](https://get.webgl.org/)

Una vez realizada esa comprobación se debe crear un archivo html que tendrá 3 partes importantes.
En el caso de esta entrada se tendrá el archivo babylon.html

### Primera Parte - Cabeceras

La etiqueta ```<head></head>``` será parecida al siguiente código
``` Html
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
    <title>Babylon - Primeros Pasos</title>
    <!---  Carga La ultima Version de Babylon.js--->
    <script src="https://cdn.babylonjs.com/babylon.js"></script>
        <!--- La parte de estilos para utilizar toda la pantalla--->
    <style>
        html, body {
            overflow: hidden;
            width   : 100%;
            height  : 100%;
            margin  : 0;
            padding : 0;
        }

        #renderCanvas {
            width   : 100%;
            height  : 100%;
            touch-action: none;
        }
    </style>
</head>
```

### Segunda Parte - Body (Html)

Se debe crear un canvas para poder recuperarlo luego en el codigo JavaScript que se escribirá más adelante.

``` Html
<canvas id="renderCanvas"></canvas>
```

### Tercera Parte - Body (Código JavaScript)

Para esta parte vamos a implementar dos escuchas de eventos el primero se encargará de cargar todos los componentes para renderizar.

El siguiente código debe ejecutarse dentro de un tag ```<script></script>```
``` JavaScript
<
        window.addEventListener('DOMContentLoaded', function(){
            // Obtiene el elemento canvas del elemento DOM
            var canvas = document.getElementById('renderCanvas');

            // Cargamos el Motor 3D
            var engine = new BABYLON.Engine(canvas, true);

            // createScene es una funcion que crea los componentes y retorna una escena
            var createScene = function(){
                // Crea un escena básica de BJS
                var scene = new BABYLON.Scene(engine);

                // Crea una camara libre y le da la posicion en un Babylon Vector
                var camera = new BABYLON.FreeCamera('camera1', new BABYLON.Vector3(0, 5,-10), scene);

                // Setea el foco de la camara en el origen de cordenadas
                camera.setTarget(BABYLON.Vector3.Zero());

                // Adjunta la camara al canvas
                camera.attachControl(canvas, false);

                // Crea una luz Hemisferica que simula el cielo y configura su posicion con un Babylon Vector
                var light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0,1,0), scene);

                // |crear una forma de "esfera" incorporada; su constructor toma 6 parámetros: name, segment, diameter, scene, updatable, sideOrientation 
                var sphere = BABYLON.Mesh.CreateSphere('sphere1', 16, 2, scene);

                // mover la esfera hacia arriba 1/2 de su altura
                sphere.position.y = 1;

                // Creamos un forma mesh que simulará un suelo.
                var ground = BABYLON.Mesh.CreateGround('ground1', 6, 6, 2, scene);

                // Retornamos la escena creada
                return scene;
            }

```

Al final se debe abrir el archivo html en un navegador y se verá algo como esto.

![](imagenes/babylonFormas.png)

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>